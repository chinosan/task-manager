type Todo = {
  is:string,  //@id @default (cuid()) @map(_id)
  title:string,
  descripction:string,
  date:string,
  isCompleted:boolean, //@default(false)
  isImportant:boolean, //@default(false)

  createdAt:Date, //DateTime @default(now()) @map("created_at")
  updatedAt:Date,//DateTime @default(now()) @map("updated-at")
  userId:string, //@map(user_id)
}