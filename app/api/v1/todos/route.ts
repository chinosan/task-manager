import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";


const prisma = new PrismaClient();

export async function GET(req:Request){
  try {
    const task = await prisma.task.create({
      data:{
        title:"tile test",
        date:"12-12-2023",
        userId:"123",
        descripction:"This si an testing todo example",
        // createdAt:"12-12-2023",
        // id:"",
        isCompleted:false,
        isImportant:false
      }
    })
    
    return NextResponse.json({message:"Task created successfully"})
  } catch (er) {
    console.log(er)
    return NextResponse.json({error:er,status:500})
  }
  
  return "Hello api";
}
export async function POST(){
  const task = await prisma.task.create({
    data:{
      title:"",
      date:"",
      userId:"123",
      descripction:"",
      createdAt:"",
      id:"",
      isCompleted:false,

    }
  })
}