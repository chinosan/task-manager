import React from 'react'

const DrawertNav = () => {
  return (
    
    <div className="flex">
      <input type="checkbox" id="drawer-toggle" className="relative sr-only peer" defaultChecked />
      <label htmlFor="drawer-toggle" className="absolute top-0 left-0 inline-block p-4 transition-all duration-500 bg-indigo-500 rounded-lg peer-checked:rotate-180 peer-checked:left-64">
        <div className="w-6 h-1 mb-3 -rotate-45 bg-white rounded-lg"></div>
        <div className="w-6 h-1 rotate-45 bg-white rounded-lg"></div>
      </label>
      <div className="fixed top-0 left-0 z-20 w-64 h-full transition-all duration-500 transform -translate-x-full bg-secondary shadow-lg peer-checked:translate-x-0">
        <div className="px-6 py-4 flex flex-col justify-between h-full">
          <div>
            <div className='rounded-full bg-slate-300 w-10 h-10' ></div>
            <h3>My user</h3>
          </div>
          <div>

          <h2 className="text-lg font-semibold">Drawer</h2>
          <p className="text-gray-500">This is a drawer.</p>
          <p className='p-2' >All</p>
          <p className='p-2' >Done</p>
          </div>
          <button className='bg-gray-600 p-2' >sing in</button>
        </div>
      </div>
      
    </div>
  )
}

export default DrawertNav