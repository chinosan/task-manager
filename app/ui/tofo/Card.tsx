import React from 'react'

const Card = () => {
  return (
    <div className='row p-4 rounded-xl ' >
      <h2 className='text-lg'>title</h2>
      <p className='text-sm'>user</p>
      <p>date: dd/mm/yyyy</p>
      <div className='flex justify-around' >
        <span className='bg-green-600 p-2 px-3 rounded-full' >status</span>
      <div>
      <button className='bg-gray-200 p-1 rounded-full' >
        <img src="/icons/Edit.svg" width={25} height={25} />
      </button>
      <button className='bg-gray-500 rounded-full p-1' >
        <img src='/icons/Delete.svg' width={25} height={25} />
      </button>

      </div>
      </div>
    </div>
  )
}

export default Card