'use client'
import React, { ChangeEvent, useState } from 'react'

const initialState = {
  title: "",
  description: "",
  date: new Date().toISOString().split("T")[0],
  isImportant: false,
  isCompleted: false
}

const NewTodo = () => {
  const [open, setOpen] = useState(false)
  const [setstatus, setSetstatus] = useState<"success"|"loading"|"error"|"default">("default");
  const [newTodo, setNewTodo] = useState(initialState);
  function handleOpen() {
    setOpen(prev => !prev)
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setNewTodo({ ...newTodo, [name]: value })
  }
  return (
    <>
      <button
        className='border-red-900 border hover:font-bold hover:bg-red-900 rounded-md transition-all'
        onClick={handleOpen}
      >
        + add new todo
      </button>
      {
        open && (
          <div
            className='z-10 p-10 absolute bg-sky-500/25 w-full h-full'
          >
            <div className='flex justify-between mb-5' >
              <button className='bg-blue-600 p-2' >Save</button>
              <button className='bg-red-500 p-1' onClick={handleOpen}>Close</button>

            </div>
            <input value={newTodo.title} onChange={handleChange} name='titulo' className='p-1 rounded-sm' placeholder='Title' type='text' />
          
            
          </div>
        )
      }
    </>
  )
}

export default NewTodo